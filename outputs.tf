output "nat.ip" {
  value = "${aws_instance.nat.public_ip}"
}
output "app.0.ip" {
  value = "${aws_instance.app.0.public_ip}"
}